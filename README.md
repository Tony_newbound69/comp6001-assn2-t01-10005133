# Tony Newbounds Individual Report

#My Part 

##Communication Tool
I was given the job of finding an adequate communication tool for us to use throughout our team project. At first I looked into some instant messaging services such as Messenger/ Viber but these were quickly dismissed  
for Slack. As we already had a slack channel with all the members apart of it including Jeff and Jacob this was a no brainer as we all use slack on a daily basis it made the decision that much simpler.  
Also with Slack the easy of ability to share files link or documentation really speaks for its self.

##WireFrames/Storyboard

After another group discission which included Jacob we decided to make a story board instead of wireframes as we feel that this would benefit us more due to the many areas that will be involved.  
After Sam (Project leader) had organised all the dimentions for each room (as close to scale as we could make it) using Visio we drew up our storyboard and added the main features to the room layout using a top down view 
I also made a second plan which shows the roof of the rooms which includes items like Lights, Projectors and so on. I suggested that we add all the rooms together from each one of our story board images and make a whole floorplan that is easy to view.

##My Assets

I had been given the rooms J140 (Communal area),J144 (Pc Dropin suit),J142 (Staffroom),J143 (Mac Dropin suit).  
I have organised my areas to maximize the learning potential giving the best floor plan whilst trying to maintain an open feeling.  
I spoke to Sam and we decided together to use the Donut symbol as a point of interest symbol this will later have information attached to it when the user select it.  
I have also provided the ceiling plan to show where the roof fixtures should be place.

